# react-halo

### Live Application
You can see the production deployed application at:
http://ec2-3-81-116-20.compute-1.amazonaws.com

### Requirements
- Ubuntu 16.04.6 LTS
- NPM 3.5.2
- Node v8.10.0
- Yarn 1.21.1

Install node
```
$ sudo apt-get install node
```

Install npm
```
$ sudo apt-get install npm
```

Install yarn

See docs: https://yarnpkg.com/lang/en/docs/install/#debian-stable

### Installation
Install dependencies
```
$ cd react-halo
$ yarn install
```
Build Application
```
$ yarn build
```

### Run
Run the application
```
$ yarn global add serve
$ serve -s build
```
