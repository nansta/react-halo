import decode from 'jwt-decode';
export default class AuthService {
    // Initializing important variables
    constructor(domain) {
        this.domain = domain || 'http://ec2-3-81-116-20.compute-1.amazonaws.com:5050/api/v1'
        this.fetch = this.fetch.bind(this)
        this.signin = this.signin.bind(this)
        this.getProfile = this.getProfile.bind(this)
    }

    signin(username, password) {
        // Get a token from api server using the fetch api
        return this.fetch(`${this.domain}/sign_in`, {
            method: 'POST',
            body: JSON.stringify({
                username,
                password
            })
        }).then(res => {
            if (res.message) {
                return Promise.reject(res);
            }
            // Set the access_token in localStorage
            this.setToken(res.access_token)
            return Promise.resolve(res);
        })
    }

    register(username, password) {
        // Get a token from api server using the fetch api
        return this.fetch(`${this.domain}/sign_up`, {
            method: 'PUT',
            body: JSON.stringify({
                username,
                password
            })
        }).then(res => {
            if (res.message) {
                return Promise.reject(res);
            }
            // Set the access_token in localStorage
            this.setToken(res.access_token)
            return Promise.resolve(res);
        })
    }

    loggedIn() {
        // Checks if there is a saved token and it's still valid
        const token = this.getToken()
        return !!token && !this.isTokenExpired(token)
    }

    isTokenExpired(token) {
        try {
            const decoded = decode(token);
            if (decoded.exp < Date.now() / 1000) {
                return true;
            }
            else
                return false;
        }
        catch (err) {
            return false;
        }
    }

    setToken(idToken) {
        // Saves user id_token to localStorage
        localStorage.setItem('id_token', idToken)
    }

    getToken() {
        // Retrieves the user id_token from localStorage
        return localStorage.getItem('id_token')
    }

    logout() {
        // Clear user id_token and profile data from localStorage
        localStorage.removeItem('id_token');
    }

    getProfile() {
        // Using jwt-decode npm package to decode the id_token
        return decode(this.getToken());
    }


    fetch(url, options) {
        // Performs api calls sending the required authentication headers
        const headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }

        // Setting Authorization header
        if (this.loggedIn()) {
            headers['Authorization'] = 'Bearer ' + this.getToken()
        }

        return fetch(url, {
            headers,
            ...options
        })
            .then(this._checkStatus)
            .then(response => response.json())
    }

    _checkStatus(response) {
        if (response.status >= 200 && response.status < 300) {
            return response
        } else {
            var error = new Error(response.statusText)
            error.response = response
            throw error
        }
    }
}