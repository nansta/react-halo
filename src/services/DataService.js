import AuthService from '../services/AuthService';

export default class DataService {
    constructor(domain) {
        this.domain = domain || 'http://ec2-3-81-116-20.compute-1.amazonaws.com:5050/api/v1'
        this.fetch = this.fetch.bind(this);
        this.get = this.get.bind(this);
        this.set = this.set.bind(this);
        this.Auth = new AuthService();
    }

    get(key) {
        return this.fetch(`${this.domain}/data/get`, {
            method: 'POST',
            body: JSON.stringify({
                key
            })
        }).then(res => {
            if (res.message) {
                return Promise.reject(res);
            }
            return Promise.resolve(res);
        })
    }

    set(key, value) {
        return this.fetch(`${this.domain}/data/set`, {
            method: 'PUT',
            body: JSON.stringify({
                key,
                value
            })
        }).then(res => {
            if (res.message) {
                return Promise.reject(res);
            }
            return Promise.resolve(res);
        })
    }

    fetch(url, options) {
        const headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }

        if (this.Auth.loggedIn()) {
            headers['Authorization'] = 'Bearer ' + this.Auth.getToken()
        }

        return fetch(url, {
            headers,
            ...options
        })
            .then(this._checkStatus)
            .then(response => response.json())
    }

}