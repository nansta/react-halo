import React, { Component } from 'react';
import AuthService from './AuthService';

export default function withAuth(AuthComponent) {
  const Auth = new AuthService('http://ec2-3-81-116-20.compute-1.amazonaws.com:5050');

  return class AuthWrapped extends Component {
    constructor() {
      super();
      this.state = {
        user: null
      }
    }

    componentDidMount() {
      if (!Auth.loggedIn()) {
        return
      }
      try {
        const profile = Auth.getProfile()
        this.setState({
            user: profile
        })
      }
      catch(err){
        Auth.logout()
        this.props.history.replace('/signin');
      }
    }

    render() {
      return (
        <AuthComponent
          history={this.props.history}
          user={this.state.user}
        />
      )
    }

  }
}