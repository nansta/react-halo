import React, { Component } from 'react'
import {
  Redirect
} from "react-router-dom";

import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Alert from 'react-bootstrap/Alert';

import 'bootstrap/dist/css/bootstrap.min.css';
import '../static/css/share.css';

import AuthService from '../services/AuthService';

export default class SignIn extends Component {
  constructor(){
    super();
    this.state = {
      errorMessage: null,
    }
    this.handleChange = this.handleChange.bind(this);
    this.handleFormSubmit = this.handleFormSubmit.bind(this);
    this.Auth = new AuthService();
  }

  errors() {
    if (this.state.errorMessage) {
      return <Alert variant="danger">{this.state.errorMessage}</Alert>
    }
  }

  render() {

    if (this.Auth.loggedIn()) {
      return <Redirect to='/' />
    }

    return (
      <Row className="Body">
        <Col md={{ span: 4, offset: 4 }}>
          {this.errors()}
          <Form
            onSubmit={(event => this.handleFormSubmit(event))}
          >
            <Form.Group controlId="formUsername">
              <Form.Label>Username</Form.Label>
              <Form.Control
                type="text"
                name="username"
                placeholder="Enter username"
                onChange={this.handleChange}
                required
              />
            </Form.Group>

            <Form.Group controlId="formPassword">
              <Form.Label>Password</Form.Label>
              <Form.Control
                type="password"
                name="password"
                placeholder="Enter Password"
                onChange={this.handleChange}
                required
              />
            </Form.Group>

            <Button
              variant="primary"
              type="submit"
            >
              Sign In
            </Button>
          </Form>
        </Col>
      </Row>
    )
  }

  handleFormSubmit(e){
    e.preventDefault();

    this.Auth.signin(
      this.state.username,
      this.state.password
    ).then(res =>{
      if (res.message) {
        this.setState({ errorMessage: res.message })
        return
      }
      this.props.history.replace('/data/get');
    }).catch(err =>{
      this.setState({ errorMessage: err.message })
    })
  }

  handleChange(e) {
    this.setState({
      [e.target.name]: e.target.value
    })
  }

}
