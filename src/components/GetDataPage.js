import React, { Component } from 'react';
import {
  Redirect
} from "react-router-dom";

import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Alert from 'react-bootstrap/Alert';

import 'bootstrap/dist/css/bootstrap.min.css';
import '../static/css/share.css';

import AuthService from '../services/AuthService';
import DataService from '../services/DataService';

export default class GetDataPage extends Component {
  constructor(){
    super();
    this.state = {
      value: null,
      errorMessage: null,
    }
    this.handleChange = this.handleChange.bind(this);
    this.handleGetData = this.handleGetData.bind(this);
    this.Auth = new AuthService();
    this.Data = new DataService();
  }

  errors() {
    if (this.state.errorMessage) {
      return <Alert variant="danger">{this.state.errorMessage}</Alert>
    }
  }

  value() {
    if (this.state.value) {
      return (
        <Col style={{ textAlign: "right" }} md={{ span: 4, offset: 4 }}>
          Value: {this.state.value}
        </Col>
      )
    }
  }

  render() {
    if (!this.Auth.loggedIn()) {
      return <Redirect to='/signin' />
    }

    return (
      <Row className="Body">
        <Col md={{ span: 4, offset: 4 }}>
          {this.errors()}
            <Form
              onSubmit={(event => this.handleGetData(event))}
            >
            <Form.Group controlId="formKey">
              <Form.Label>Key</Form.Label>
              <Form.Control
                type="text"
                name="key"
                placeholder="Enter key"
                onChange={this.handleChange}
                required
              />
            </Form.Group>

            <Button
              variant="primary"
              type="submit"
              onClick={this.handleGetData.bind(this)}
            >
              Get
            </Button>
          </Form>
        </Col>
        {this.value()}
      </Row>
    )
  }

  handleGetData(e) {
    e.preventDefault();

    this.Data.get(
      this.state.key
    ).then(res =>{
      if (res.message) {
        this.setState({ errorMessage: res.message })
        return
      }
      this.setState({ value: res.value })
      this.setState({ errorMessage: null })
    }).catch(err =>{
      this.setState({ errorMessage: err.message })
    })
  }

  handleChange(e) {
    this.setState({
      [e.target.name]: e.target.value
    })
  }

}

