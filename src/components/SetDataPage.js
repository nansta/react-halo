import React, { Component } from 'react';
import {
  Redirect
} from "react-router-dom";

import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Alert from 'react-bootstrap/Alert';

import 'bootstrap/dist/css/bootstrap.min.css';
import '../static/css/share.css';

import AuthService from '../services/AuthService';
import DataService from '../services/DataService';

export default class extends Component {
  constructor(){
    super();
    this.state = {
      success: false,
      errorMessage: null,
    }
    this.handleChange = this.handleChange.bind(this);
    this.handleSetData = this.handleSetData.bind(this);
    this.Auth = new AuthService();
    this.Data = new DataService();
  }

  errors() {
    if (this.state.errorMessage) {
      return <Alert variant="danger">{this.state.errorMessage}</Alert>
    }
  }

  success() {
    if (this.state.success) {
      return <Alert variant="success">Success!</Alert>
    }
  }

  render() {
    if (!this.Auth.loggedIn()) {
      return <Redirect to='/signin' />
    }

    return (
      <Row className="Body">
        <Col md={{ span: 4, offset: 4 }}>
          {this.errors()}
          {this.success()}
          <Form
            onSubmit={(event => this.handleSetData(event))}
          >
            <Form.Group controlId="formKey">
              <Form.Label>Key</Form.Label>
              <Form.Control
                type="text"
                name="key"
                placeholder="Enter key"
                onChange={this.handleChange}
                required
              />
            </Form.Group>

            <Form.Group controlId="formVal">
              <Form.Label>Value</Form.Label>
              <Form.Control
                type="text"
                name="value"
                placeholder="Enter value"
                onChange={this.handleChange}
                required
              />
            </Form.Group>

            <Button
              variant="primary"
              type="submit"
            >
              Set
            </Button>
          </Form>
        </Col>
      </Row>
    )
  }

  handleSetData(e) {
    e.preventDefault();

    this.Data.set(
      this.state.key,
      this.state.value
    ).then(res =>{
      if (res.message) {
        this.setState({ errorMessage: res.message })
        return
      }
      this.setState({ success: true })
    }).catch(err =>{
      this.setState({ errorMessage: err.message })
    })
  }

  handleChange(e) {
    this.setState({
      [e.target.name]: e.target.value
    })
  }

}

