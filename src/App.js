import React, {Component} from 'react';
import {
  withRouter,
  Switch,
  Route,
} from "react-router-dom";

import Container from 'react-bootstrap/Container';

import HaloNavbar from './common/HaloNavbar';
import SetDataPage from './components/SetDataPage';
import GetDataPage from './components/GetDataPage';
import SignIn from './components/SignIn';
import Register from './components/Register';
import { createBrowserHistory } from 'history';
import 'bootstrap/dist/css/bootstrap.min.css';

import AuthService from './services/AuthService';
import withAuth from './services/withAuth';

class App extends Component {
  constructor(){
    super();
    this.history = createBrowserHistory();
    this.path = (/#!(\/.*)$/.exec(this.history.location.hash) || [])[1];
    this.Auth = new AuthService();
  }

  componentDidMount() {
    if (this.path) {
      this.history.replace(this.path);
    }
  }

  render() {
    return (
      <Container>
        <HaloNavbar
          history={this.props.history}
          user={this.props.user}
        ></HaloNavbar>
        <Switch>
            <Route
              path="/data/get"
              component={GetDataPage}
            />
            <Route
              path="/data/set"
              component={SetDataPage}
            />
            <Route
              path="/signin"
              component={SignIn}
            />
            <Route
              path="/register"
            >
              <Register
                history={this.props.history}
                user={this.props.user}
              ></Register>
            </Route>
          </Switch>
      </Container>
    );
  }
}

export default withRouter(withAuth(App));