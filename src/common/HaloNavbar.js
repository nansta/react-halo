import React, { Component } from 'react';

import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';

import 'bootstrap/dist/css/bootstrap.min.css';

import AuthService from '../services/AuthService';

export default class HaloNavbar extends Component {
  constructor(props){
    super(props);
    this.Auth = new AuthService();
  }

  links() {
    if (!this.Auth.loggedIn()) {
      return(
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto">
          </Nav>
          <Nav>
            <Nav.Link href="/signin">Sign In</Nav.Link>
            <Nav.Link href="/register">Register</Nav.Link>
          </Nav>
        </Navbar.Collapse>
      )
    }
    return (
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="mr-auto">
          <Nav.Link href="/data/get">Get</Nav.Link>
          <Nav.Link href="/data/set">Set</Nav.Link>
        </Nav>
        <Nav>
          <Nav.Link onClick={this.handleLogout.bind(this)}>Logout</Nav.Link>
        </Nav>
      </Navbar.Collapse>
    )
  }

  render() {
    return (
      <Row>
        <Col>
          <Navbar bg="dark" variant="dark">
          <Navbar.Brand href="/">Halo</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          {this.links()}
          </Navbar>
        </Col>
      </Row>
    )
  }

  handleLogout() {
    this.Auth.logout()
    this.props.history.replace('/signin')
  }

}
